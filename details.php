<?php 
// include("inc/data.php");
// $catalog = full_catalog_array();
include("inc/functions.php");

if(isset($_GET["id"])){
    $id = filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);

    $item = single_item_array($id);
}
    if(empty($item)){
        echo "Nothing";
        header("location: catalog.php");
        exit;
    }
    $pageTitle = $item["name"];
    
    $section = null;

include("inc/header.php"); 
?>

<div class="section2">
    <div id="display_name">
        <h3><span><?php echo $item["name"];?></span></h3>
    </div>

    <div class="container clearfix">
        <div class = "breadcrumbs"> 
            <a href ="catalog.php">Full Catalog</a>
            &gt; <a  href = "catalog.php?cat=<?php echo strtolower($item["category"]); ?>">
            <?php echo $item["category"]; ?> </a>
            &gt; <?php echo $item["name"];?>
        </div>
        <div class="details imgcol">
            <img class="details-img" src = "<?php echo $item["img"];?>" alt = "<?php echo $item["name"];?>" />
            <p id="disclaimer">We, at Anne&rsquo;s Cakes understand that every ocassion is unique and different. 
            We customise our cake designs based on the ocassion and customer requirements. Each 
            product featured on the website catalogue may vary in availability, design and price. Please discuss your specific 
            requirements with us to help us add more delight and fun to your ocassions.</p>
        </div>

       <div class="details desccol">
            <p><?php echo $item["description"]; ?></p>
            <table>
                <tr>
                    <th>Product Servings:</th>
                    <td><?php echo $item["servings"];?></td>
                </tr>
                <tr>
                    <th>Price From:</th>
                    <td><?php echo "$".$item["price"];?></td>
                </tr>
            </table>
            <!--<p>With a baby on board, this brightly decorated Boston Mudcake is ideal for 
                baby showers and birthdays.</p>
            <table>
                <tr>
                    <td>Product Servings:</td>
                    <td>48-60</td>
                </tr>
                <tr>
                    <td>Price From:</td>
                    <td>$95.90</td>
                </tr>
            </table>-->
       </div>
        
    </div class="clearfix">
</div> <!-- /section2 -->
<?php include("inc/footer.php"); ?>