<?php 
include("inc/functions.php");

$pageTitle = "Full Catalog";
$section = null;
$search = null;
$items_per_page = 6;

if(isset($_GET["cat"])){
    if($_GET["cat"]=="kids"){
    $pageTitle = "Kids Cakes";
    $section = "kids";
    }
    else if($_GET["cat"]=="celebration"){
        $pageTitle = "Celebration Cakes";
        $section = "celebration";
    }
    else if($_GET["cat"]=="theme"){
        $pageTitle = "Theme Cakes";
        $section = "theme";
    }
}
if( isset($_GET["pg"])){
    $current_page = FILTER_INPUT(INPUT_GET, "pg", FILTER_SANITIZE_NUMBER_INT);
}
if( isset($_GET["s"])){
    $search = FILTER_INPUT(INPUT_GET, "s", FILTER_SANITIZE_STRING);
}
if(empty($current_page)){
    $current_page = 1;
}
$total_items = get_catalog_count($section,$search);
$total_pages = 1;
$offset = 0;
if($total_items > 0){

    $total_pages = ceil($total_items/$items_per_page);
    // include("inc/data.php");

    //Filter results in redirect. - if in a category page.
    $filter_results = "";
    if(!empty($search)){
        $filter_results = "s=".urlencode(htmlspecialchars($search))."&";
    }
    else if(!empty($section)){
        $filter_results = "cat =".$section."&";
    }


    //Redirect too large page nos to the last pg
    if($current_page > $total_pages){
        header("location: catalog.php?".
        $filter_results.
        "pg=".$total_pages);
    }
    //Redirect too small page nos to the first pg
    if($current_page < 1){
        header("location: catalog.php?".
        $filter_results.
        "pg=1");
    }
    //determine the offset , which is the no of items to skip for the current password_get_info
    //FOr eg, for page3 we need to skip 16 items

    $offset = ($current_page-1)*$items_per_page;

    $pagination = "<div class=\"pagination\">";
    $pagination.= "Pages: ";
            
            
    for($i=1;$i<=$total_pages;$i++){
            if($i == $current_page){
                $pagination.=  "<span> $i</span>";
            }
            else{
                $pagination.= "<a href='catalog.php?";
                if(!empty($search)){
                    $pagination.= "s=".urlencode(htmlspecialchars($search))."&";
                }
                else if(!empty($section)){
                    $pagination.= "cat=".$section."&";
                }
                $pagination.= "pg=$i'> $i</a>";
            }
        }
    $pagination.="</div>";
}
if(!empty($search)){
    $catalog = search_catalog_array($search, $items_per_page,$offset);
}
else if(empty($section)){
    $catalog = full_catalog_array($items_per_page,$offset);
}else {
   $catalog = category_catalog_array($section,$items_per_page,$offset);
}


include("inc/header.php"); ?>

<div class="section2">
    <div class="container">

        <h3><?php
            if($search != null){
                echo "Search Results for \"".htmlspecialchars($search)."\"";
            }
        else {
          if ($section != null) {
            echo "<a href='catalog.php'>Full Catalog</a> &gt; ";
          }
          echo $pageTitle;
        }
         ?></h3>
         <?php 
         if($total_items < 1){
             echo "<p>No items were found matching that search term.</p>";
             echo "<p>Search again or ".
             "<a href=\"catalog.php\">Browse the full catalog.</a></p>";
         }
         else{
         echo $pagination; ?>
        
        
        <ul class="items clearfix">
           <?php 
            // $categories = array_category($catalog, $section); 
            //returns an array of keys for the items with specified category.

            foreach($catalog as $item){                        
                echo get_item_html($item);
            }?>
        </ul>
        <?php echo $pagination; 
         } ?>
    </div>
</div> <!--/Section2 -->

<?php include("inc/footer.php"); ?>