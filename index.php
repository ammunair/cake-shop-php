<?php

$pageTitle = "Cakes designed and made with love and passion!";
$section = null;
// include("inc/data.php");
include("inc/functions.php");
// $catalog = full_catalog_array();
include("inc/header.php");
?>


<div class="section2">
    <div class="container">
        <h3><?php echo $pageTitle; ?></h3>
        <ul class="items clearfix">
            <?php 
            $random = random_catalog_array(); //returns an array of keys for the random items
            foreach($random as $item){
                echo get_item_html($item);
            }?>
        </ul>
    </div>
</div> <!--/section2 -->

<div class="section1">
    <div class="container clearfix">
        <!--
        <div class="col1">
            <img src="img/cake2.jpg">
        </div>
        <div class="col2">
            <p>No matter what the occasion, Annes Cakes has the perfect celebration cake. There is 
        no better way to celebrate your birthday, engagement, wedding, anniversary, work function or any 
        other gathering of friends and family than with a custom designed celebration cake from Annes. With our 
        licensed cakes, you can even celebrate with famous friends including Angry Birds, Play School, Giggle & Hoot, 
        Scooby Doo or Thomas the Tank Engine. Order online today and make planning your next occasion a breeze.
        </p>
        </div>
        <div class="col3">
            <img src="img/cake4.jpg" >
        </div>
        -->
       <div>
       <h3>Our custome made cakes are sure to impress all your guests.</h3>
        <p>No matter what the occasion, <span class="stylefont">Anne&rsquo;s Cakes</span> has the perfect celebration cake. There is 
        no better way to celebrate your birthday, engagement, wedding, anniversary, work function or any 
        other gathering of friends and family than with a custom designed celebration cake from Annes. With our 
        licensed cakes, you can even celebrate with famous friends including Angry Birds, Play School, Giggle & Hoot, 
        Scooby Doo or Thomas the Tank Engine. </p>
        <p>
        We understand that your cakes have to just fabulous, like your occasions are. SO we wrok closely with 
        every customer to create a unique and delicious cake for every ocassion. 
        </p>
        <h3>Say happy birthday with our custom-designed cakes</h3>
        <p>
            If you want to impress your guests, look no further than our cakes. We are dedicated to making your
            celebrations special, and love creating ckaes for those major milestones - whether it be a 21<sup>st</sup>
            party, a 50th orr a 100th. 
        </p>
        <p>We also love childrens parties - between the balloons, the dancing and the streamers, we cant
        seem to get enough of the fun. And what better way to top it off than with a perfect birthday cake
        for your son or daughter that matches their special interests - or an impressive 3D cake to bring
        the party to life.</p>

        <h3>MAKE YOUR WEDDING DAY TIMELESS</h3>

        <p>You want to make your once-in-a-lifetime union an impeccable day to remember. 
        Our designers create elegant masterpieces especially for you, so you can say <span class="stylefont">“I do”</span> in 
        the most stylish, delicious way. </p>

        <p>No matter what your special cake needs or occasion, we have you covered.</p>
       </div>
        <div class="clearfix"> </div>
            
    </div>
</div> <!--/section1 --> 

<?php include("inc/footer.php"); ?>
       
<script src="http://code.jquery.com/jquery-1.11.0.min.js" type="text/javascript" charset="utf-8"></script>
        <script> 
            $(document).ready(function(){
                $("nav #pull").on("click", function(e){
                    e.preventDefault();
                    $("nav ul li:not(#pull)").slideToggle();
                });
            });
</script>