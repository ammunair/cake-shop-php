<?php 

if($_SERVER["REQUEST_METHOD"] == "POST"){
    $name = trim(filter_input(INPUT_POST, "name",FILTER_SANITIZE_STRING));
    $email = trim(filter_input(INPUT_POST, "email",FILTER_SANITIZE_EMAIL));
    $details = trim(filter_input(INPUT_POST, "details",FILTER_SANITIZE_SPECIAL_CHARS));

    if ($name == "" OR $email =="" OR $details ==""){
        $error_message ="Please fill in the required fields, Name, email, details.";
        // echo "Please fill in the required fields, Name, email, details.";
    }
    require("inc/phpmailer/class.phpmailer.php");
    $mail = new PHPMailer;

    if (!isset($error_message) && !$mail->ValidateAddress($email)) {
        $error_message = "Invalid Email Address";
    }
    if(!isset($error_message)){
        $email_body ="";
        $email_body .= "Name ".$name."\n";
        $email_body .= "Email: ".$email."\n";
        $email_body .= "Details: ".$details."\n";
        echo $email_body;

        $mail->setFrom($email, $name);
        $mail->addAddress('treehouse@localhost', 'Alena');     // Add a recipient
        
        $mail->isHTML(false);                                  // Set email format to HTML
        
        $mail->Subject = 'Personal Media Library Suggestion from ' . $name;
        $mail->Body    = $email_body;

        if($mail->send()){
            //Send email
            header("location:suggest.php?status=thanks");
        }
        $error_message = 'Message could not be sent.';
        $error_message .= 'Mailer Error: ' . $mail->ErrorInfo;
    }
    
}

$section = "contact";
$pageTitle = "Contact Us";
include("inc/header.php"); ?>

<div class="section2">
    <div class="container">
        
        <?php if (isset($_GET["status"]) && $_GET["status"] == "thanks") {
            echo "<p>Thanks for the email! We&rsquo;ll get back to you shortly.</p>";
        } 
        else { //if mail was not send,  either display error message, or display form.
            if (isset($error_message)) {
                echo "<p class='message'>".$error_message . "</p>";
            }   
            else {
                echo "<p class='suggest'>Have a great design in mind?</p> <p class='suggest'>Share it with us.</p>";
            }
        ?>

            <form method="post" action = "suggest.php">
            <table>
                <tr>
                    <th><label for = "name">Name</label></th>
                    <td><input type="text" name="name" id="name"></td>
                </tr>

                <tr>
                    <th><label for = "email">Email: </label></th>
                    <td><input type="text" name="email" id="email"></td>
                </tr>
                <tr>
                    <th><label for = "details"> Details:  </label></th>
                    <td><textarea name="details" id="details"></textarea></td>
                </tr>
            </table>
            <input type="submit" value="Send" >  
            
            </form>

        <?php } ?>
        
    </div>
</div>

<?php include("inc/footer.php"); ?>