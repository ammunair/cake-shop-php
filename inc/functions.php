<?php

function get_catalog_count($category = null,$search = null) {
    $category = strtolower($category);
    include("connection.php");

    try {
        $sql = "SELECT COUNT(cake_id) FROM cakes";
        if (!empty($search)) {
          $result = $db->prepare(
            $sql
            . " WHERE name LIKE ?");
        //   echo "This is ".$sql;
          $result->bindValue(1,'%'.$search.'%',PDO::PARAM_STR);
        } else if (!empty($category)) {
          $result = $db->prepare(
            $sql
            . " WHERE LOWER(category) = ?");
        //   echo "This is cat ".$sql;
          $result->bindParam(1,$category,PDO::PARAM_STR);
        } else {
          $result = $db->prepare($sql);
        }
        $result->execute();
    } catch (Exception $e) {
      echo "bad query";
    }
  
  $count = $result->fetchColumn(0);
  return $count;
}


// function get_catalog_count($category = null, $search = null){
//     echo $category;
//     echo $search;
//     include("inc/connection.php");
//     $category = strtolower($category);
//     try{
//         $sql = "SELECT count(cake_id) FROM Cakes";
//         if(!empty($search)){
//             $result = $db->prepare($sql." WHERE name LIKE ?");
//             $result->bindValue(1,'%'.$search.'%',PDO::PARAM_STR);
//         }
//         else if(!empty($category)){
            
//             $result = $db->prepare($sql."WHERE lower(category) = ?");
//             $result->bindParam(1,$category,PDO::PARAM_STR);
//         }   
//         else{
//             echo $result;
//             $result = $db->prepare($sql);
//         }
            
//         $result->execute();
//     }
//         catch(Exception $e){
//             echo "Bad Query";
//             echo $e->getMessage();
//         }
//         $count = $result->fetchColumn(0);
//         return $count;
// }
function full_catalog_array($limit=null,$offset=0){ //$limit is the no of items per page
    include("inc/connection.php");
    try{
    $sql = ("SELECT cake_id, name, category, img 
                            FROM cakes 
                            ORDER BY 
                            REPLACE(
                             REPLACE(
                                REPLACE(name,'The ',''),
                                'An ',''),
                                'A ','')"
                                );
    if(is_integer($limit)){
        $results = $db->prepare($sql."LIMIT ? OFFSET ?");
        $results->bindParam(1,$limit,PDO::PARAM_INT);
        $results->bindParam(2,$offset,PDO::PARAM_INT);
    }
    else{
        $results = $db->prepare($sql);
    }
    $results->execute();
    }
    catch(Exception $e){
        echo "Unable to retrieve results";
        exit;
    }
    $catalog = $results->fetchAll(PDO::FETCH_ASSOC);
    return $catalog;
    //var_dump($catalog);
    //var_dump($results->fetchAll(PDO::FETCH_ASSOC));
}

function category_catalog_array($category, $limit=null, $offset=0){
    include("inc/connection.php");
    try{
        $category=strtolower($category);
        $sql=(
            "SELECT cake_id, name, category, img 
            FROM cakes 
            WHERE LOWER(category) = ?
            ORDER BY 
            REPLACE(
                REPLACE(
                    REPLACE(name,'The ',''),
                    'An ',''),
                'A ','')"
            );
        if(is_integer($limit)){
            $results = $db->prepare($sql."LIMIT ? OFFSET ?");
            $results->bindParam(1,$category,PDO::PARAM_STR);
            $results->bindParam(2,$limit,PDO::PARAM_INT);
            $results->bindParam(3,$offset,PDO::PARAM_INT);
        }
        else{
            $results = $db->prepare($sql);
            $results->bindParam(1,$category,PDO::PARAM_STR);
        }
        
        $results->execute();
    }
    catch(Exception $e){
        echo "Unable to retrieve results";
        exit;
    }
    $catalog = $results->fetchAll(PDO::FETCH_ASSOC);
    return $catalog;
    //var_dump($catalog);
    //var_dump($results->fetchAll(PDO::FETCH_ASSOC));
}
function search_catalog_array($search, $limit=null, $offset=0){
    include("inc/connection.php");
    try{
        
        $sql=(
            "SELECT cake_id, name, category, img 
            FROM cakes 
            WHERE name LIKE ?
            ORDER BY 
            REPLACE(
                REPLACE(
                    REPLACE(name,'The ',''),
                    'An ',''),
                'A ','')"
            );
        
        if(is_integer($limit)){
            $results = $db->prepare($sql."LIMIT ? OFFSET ?");
            $results->bindValue(1,'%'.$search.'%',PDO::PARAM_STR);
            $results->bindParam(2,$limit,PDO::PARAM_INT);
            $results->bindParam(3,$offset,PDO::PARAM_INT);
        }
        else{
            $results = $db->prepare($sql);
            $results->bindValue(1,'%'.$search.'%',PDO::PARAM_STR);
        }
        
        $results->execute();
    }
    catch(Exception $e){
        echo "Unable to retrieve results";
        exit;
    }
    $catalog = $results->fetchAll(PDO::FETCH_ASSOC);
    return $catalog;
    //var_dump($catalog);
    //var_dump($results->fetchAll(PDO::FETCH_ASSOC));
}
function random_catalog_array(){
    include("inc/connection.php");
    try{
        $results = $db->query("SELECT cake_id, name,
                            category, img 
                            FROM Cakes
                            ORDER BY RAND()
                            LIMIT 3");
        // echo "Results retrieved from database";
    }
    catch(Exception $e){
        echo "Unable to retrieve results";
        exit;
    }
    $catalog = $results->fetchAll(PDO::FETCH_ASSOC); //This returns the databse results as an array.
    return $catalog;
}

function single_item_array($id){
    include("inc/connection.php");
    try{
        $results = $db->prepare("SELECT name, description,category,price,servings, img 
                    FROM Cakes where cake_id = ?");
        //echo "Results retrieved from database";
        $results->bindParam(1,$id,PDO::PARAM_INT);
        $results->execute();
    }
    catch(Exception $e){
        echo "Unable to retrieve results";
        exit;
    }
    $catalog = $results->fetch(PDO::FETCH_ASSOC); //This returns the databse results as an array.
    // if (empty($item)) return $item;
    return $catalog;
}

function get_item_html($item){ 
    //Given an item of the catalog, this returns the html to display the item in a ul
    $output = "<li><a href='details.php?id=".$item["cake_id"]."'> 
                <img src='".$item["img"].
                    "'alt = '".$item["name"]."'/>".
                "<p>View Details</p></a></li>";
    return $output;
}

function array_category($catalog, $category){
    //To return an array of keys for items that belongs to a specified array_category.

//Return all array keys if section is null
// if($category == null){ 
//     return array_keys($catalog);
// }
    $output = [];
    foreach($catalog as $id => $item){
        if($category == null OR strtolower($item["category"]) == strtolower($category)){
            $sort = $item["name"];
            $sort = ltrim($sort,"The ");
            $sort = ltrim($sort,"A ");
            $sort = ltrim($sort,"An ");
            $output[$id] = $sort; // output is an asso array with the name and id of each item.
        }
    }
    asort($output);
    return array_keys($output);
}
?>