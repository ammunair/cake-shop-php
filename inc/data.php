<?php

$catalog = [];
include("inc/connection.php");
//Category kids
/*
$catalog[101] = [
    "name" => "Spot Minnie Mouse!",
    "description" => "Celebrate with a delicious cake of your choice covered with fresh cream, topped with fondant Minnie Mouse inspired decorations and personalised message.",
    "category" => "Kids",
    "price" => 210.00,
    "servings" => "55-60",
    "img"   => "img/cakes/minnie.jpg"
];
$catalog[102] = [
    "name" => "Princess Rose",
    "description" => "Spoil your little princess with a mudcake of your choice encased in pretty pink coloured chocolate ganache roses and topped with an edible image.",
    "category" => "Kids",
    "price" => 95.90,
    "servings" => "30-40",
    "img"   => "img/cakes/rose.jpg"
];
$catalog[103] = [
    "name" => "Croc Rocks",
    "description" => "Fun Crocodile rocks with hand piped deliciousness. Layered over our super cream filled tortes or cake of choice.",
    "category" => "Kids",
    "price" => 125.95,
    "servings" => "40",
    "img"   => "img/cakes/croc.jpg"
];
$catalog[104] = [
    "name" => "Spidy Sense",
    "description" => "Get in touch with your 'spidey sense' by enjoying this deliciously moist White Gold Mudcake decorated in white chocolate and red marbled icing, and topped with a few eight legged friends.",
    "category" => "Kids",
    "price" => 72.90,
    "servings" => "25-30",
    "img"   => "img/cakes/spidey.jpg"
];

$catalog[105] = [
    "name" => "Floral Fantasy",
    "description" => "An enticing three tiered White Gold Mudcake covered in smooth white chocolate topping. Decorated with an elegant flower arrangement and finished with dusty pink ribbon surrounding each tier.",
    "category" => "Kids",
    "price" => 299.70,
    "servings" => "100",
    "img"   => "img/cakes/floral.jpg"
];


//Category celebration
$catalog[201] = [
    "name" => "I Love You",
    "description" => "Love really is all around with a sculpted heart shaped mud cake. Iced in decadent chocolate ganache, topped with romantic red ganache rosettes for your sweetheart.",
    "category" => "Celebration",
    "price" => 33.95,
    "servings" => "20-30",
    "img"   => "img/cakes/love_you.jpg"
];
$catalog[202] = [
    "name" => "Butterfly Cascade",
    "description" => "A three tiered White Gold Mudcake covered in melt-in-the-mouth white chocolate topping. Decorated with a cascade of beautiful butterflies, and finished with lilac ribbon and exquisite white chocolate piping surrounding the base of each tier.",
    "category" => "Celebration",
    "price" => 299.70,
    "servings" => "100",
    "img"   => "img/cakes/butterfly.jpg"
];
$catalog[203] = [
    "name" => "Baby on Board",
    "description" => "With a baby on board, this brightly decorated Boston Mudcake is ideal for baby showers and birthdays.",
    "category" => "Celebration",
    "price" => 95.90,
    "servings" => "48-60",
    "img"   => "img/cakes/baby_board.jpg"
];
$catalog[204] = [
    "name" => "Baby Boom",
    "description" => "Cute and classic, this sculpted mudcake is the perfect addition to your baby shower.",
    "category" => "Celebration",
    "price" => 300,
    "servings" => "30-40",
    "img"   => "img/cakes/baby_boom.jpg"
];
$catalog[205] = [
    "name" => "White Birthday",
    "description" => "Simple and sweet. Our classic white gold mudcake with white chocolate ganache. Price includes writing, chocolate curls, piping and board.Additional options include ribbons, flowers and other decorations.",
    "category" => "Celebration",
    "price" => 175.90,
    "servings" => "80",
    "img"   => "img/cakes/white.jpg"
];
$catalog[206] = [
    "name" => "Cupid's Arrow",
    "description" => "Show how much you love each other with this two tiered White Gold Mudcake coated in smooth white chocolate topping. Decorated with white chocolate piping and finished with a bouquet of red roses, ribbon and Cupid ornament.",
    "category" => "Celebration",
    "price" => 189.90,
    "servings" => "55-60",
    "img"   => "img/cakes/cupid.jpg"
];
$catalog[206] = [
    "name" => "Swirl of Roses",
    "description" => "Celebrate with a swirl of rosettes encasing a layered mud cake of your choice.",
    "category" => "Celebration",
    "price" => 72.90,
    "servings" => "25-30",
    "img"   => "img/cakes/roses.jpg"
];

//Category Theme
$catalog[301] = [
    "name" => "Up in the Air",
    "description" => "Take a trip with this delicious cake of your choice. A stylish handbag cake with accessories including an edible shoe, ticket, passport and margarita.",
    "category" => "Theme",
    "price" => 245,
    "servings" => "20-25",
    "img"   => "img/cakes/air.jpg"
];
$catalog[302] = [
    "name" => "Batman Begins",
    "description" => "Save the day with a Batman themed mudcake of your choice covered with smooth fondant.",
    "category" => "Theme",
    "price" => 155,
    "servings" => "40",
    "img"   => "img/cakes/batman.jpg"
];
$catalog[303] = [
    "name" => "Footy Field",
    "description" => "Kick a Goal with this delicious cake of your choice, expertly decorated with smooth, green fondant, topped with soccer player figurines and goals. The perfect cake for your Football Friend.",
    "category" => "Theme",
    "price" => 145,
    "servings" => "30-40",
    "img"   => "img/cakes/footy.jpg"
];
$catalog[304] = [
    "name" => "Princess Cinderelly",
    "description" => "Our take on the classic Dolly Varden, exquisitely decorated with a pretty party gown created from fondant.",
    "category" => "Theme",
    "price" => 130,
    "servings" => "15-25",
    "img"   => "img/cakes/cinderelly.jpg"
];
*/
// $catalog[101] = [
// 	"title" => "A Design Patterns: Elements of Reusable Object-Oriented Software",
// 	"img" => "img/media/design_patterns.jpg",
//     "genre" => "Tech",
//     "format" => "Paperback",
//     "year" => 1994,
//     "category" => "Books",
//     "authors" => [
//         "Erich Gamma",
//         "Richard Helm",
//         "Ralph Johnson",
//         "John Vlissides"
//     ],
//     "publisher" => "Prentice Hall",
//     "isbn" => '978-0201633610'
// ];
// $catalog[102] = [
//     "title" => "Clean Code: A Handbook of Agile Software Craftsmanship",
//     "img" => "img/media/clean_code.jpg",
//     "genre" => "Tech",
//     "format" => "Ebook",
//     "year" => 2008,
//     "category" => "Books",
//     "authors" => [
//         "Robert C. Martin"
//     ],
//     "publisher" => "Prentice Hall",
//     "isbn" => '978-0132350884'
// ];
// $catalog[103] = [
//     "title" => "Refactoring: Improving the Design of Existing Code",
//     "img" => "img/media/refactoring.jpg",
//     "genre" => "Tech",
//     "format" => "Hardcover",
//     "year" => 1999,
//     "category" => "Books",
//     "authors" => [
//         "Martin Fowler",
//         "Kent Beck",
//         "John Brant",
//         "William Opdyke",
//         "Don Roberts"
//     ],
//     "publisher" => "Addison-Wesley Professional",
//     "isbn" => '978-0201485677'
// ];
// $catalog[104] = [
//     "title" => "The Clean Coder: A Code of Conduct for Professional Programmers",
//     "img" => "img/media/clean_coder.jpg",
//     "genre" => "Tech",
//     "format" => "Audio",
//     "year" => 2011,
//     "category" => "Books",
//     "authors" => [
//         "Robert C. Martin"
//     ],
//     "publisher" => "Prentice Hall",
//     "isbn" => '007-6092046981'
// ];
// //Movies
// $catalog[201] = [
//     "title" => "Forrest Gump",
//     "img" => "img/media/forest_gump.jpg",
//     "genre" => "Drama",
//     "format" => "DVD",
//     "year" => 1994,
//     "category" => "Movies",
//     "director" => "Robert Zemeckis",
//     "writers" => [
//         "Winston Groom",
//         "Eric Roth"
//     ],
//     "stars" => [
//         "Tom Hanks",
//         "Rebecca Williams",
//         "Sally Field",
//         "Michael Conner Humphreys"
//     ]
// ];
// $catalog[202] = [
//     "title" => "Office Space",
//     "img" => "img/media/office_space.jpg",
//     "genre" => "Comedy",
//     "format" => "Blu-ray",
//     "year" => 1999,
//     "category" => "Movies",
//     "director" => "Mike Judge",
//     "writers" => [
//         "William Goldman"
//     ],
//     "stars" => [
//         "Ron Livingston",
//         "Jennifer Aniston",
//         "David Herman",
//         "Ajay Naidu",
//         "Diedrich Bader",
//         "Stephen Root"
//     ]
// ];
// $catalog[203] = [
//     "title" => "The Lord of the Rings: The Fellowship of the Ring",
//     "img" => "img/media/lotr.jpg",
//     "genre" => "Drama",
//     "format" => "Blu-ray",
//     "year" => 2001,
//     "category" => "Movies",
//     "director" => "Peter Jackson",
//     "writers" => [
//         "J.R.R. Tolkien",
//         "Fran Walsh",
//         "Philippa Boyens",
//         "Peter Jackson"
//     ],
//     "stars" => [
//         "Ron Livingston",
//         "Jennifer Aniston",
//         "David Herman",
//         "Ajay Naidu",
//         "Diedrich Bader",
//         "Stephen Root"
//     ]
// ];
// $catalog[204] = [
//     "title" => "The Princess Bride",
//     "img" => "img/media/princess_bride.jpg",
//     "genre" => "Comedy",
//     "format" => "DVD",
//     "year" => 1987,
//     "category" => "Movies",
//     "director" => "Rob Reiner",
//     "writers" => [
//         "William Goldman"
//     ],
//     "stars" => [
//         "Cary Elwes",
//         "Mandy Patinkin",
//         "Robin Wright",
//         "Chris Sarandon",
//         "Christopher Guest",
//         "Wallace Shawn",
//         "André the Giant",
//         "Fred Savage",
//         "Peter Falk",
//         "Billy Crystal"
//     ]
// ];
// //Music
// $catalog[301] = [
//     "title" => "Beethoven: Complete Symphonies",
//     "img" => "img/media/beethoven.jpg",
//     "genre" => "Clasical",
//     "format" => "CD",
//     "year" => 2012,
//     "category" => "Music",
//     "artist" => "Ludwig van Beethoven"
// ];
// $catalog[302] = [
//     "title" => "Elvis Forever",
//     "img" => "img/media/elvis_presley.jpg",
//     "genre" => "Rock",
//     "format" => "Vinyl",
//     "year" => 2015,
//     "category" => "Music",
//     "artist" => "Elvis Presley"
// ];
// $catalog[303] = [
//     "title" => "No Fences",
//     "img" => "img/media/garth_brooks.jpg",
//     "genre" => "Country",
//     "format" => "Cassette",
//     "year" => 1990,
//     "category" => "Music",
//     "artist" => "Garth Brooks"
// ];
// $catalog[304] = [
//     "title" => "The Very Thought of You",
//     "img" => "img/media/nat_king_cole.jpg",
//     "genre" => "Jaz",
//     "format" => "MP3",
//     "year" => 2008,
//     "category" => "Music",
//     "artist" => "Nat King Cole"
// ];