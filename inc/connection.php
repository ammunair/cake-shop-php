<?php
try{
    $db = new PDO("mysql:host=localhost;dbname=cakeshop;port=3306","root","root");
    $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION); // To raise an exception whenever something is wrong
    //for warnings too.
}
catch(Exception $e){
    echo "Cannot connect to database";
    echo $e->getMessage(); //Displays error msg. getMessage is a method from php exception class.
    exit;
}
//echo "Connected successfully"."<br>";
?>