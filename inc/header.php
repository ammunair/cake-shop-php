<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo $pageTitle;?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link href='https://fonts.googleapis.com/css?family=Niconne' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class = "header">
           <h1>Anne&rsquo;s Cake Shop</h1>
            <nav>  
                <ul>
                    <li id="pull"><a href="#"><img src="img/nav-icon.png"></a></li>
                    <!--<li id="pull"><a href="#">&#9776</a></li>-->
                    <li><a href="index.php">Home</a></li>
                    <li><a href="catalog.php?cat=kids" class="<?php if($section=="kids"){echo ' on';}?>">Kids Cakes</a></li>
                    <li><a href="catalog.php?cat=celebration" class="<?php if($section=="celebration"){echo ' on';}?>">Celebration cakes</a></li>
                    <li><a href="catalog.php?cat=theme" class="<?php if($section=="theme"){echo ' on';}?>">Theme Cakes</a></li>
                    <li><a href="suggest.php?cat=contact" class="<?php if($section=="contact"){echo ' on';}?>">Contact Us</a></li>
                </ul>
            </nav>
        </div>
       <div class="search">
           <form id="search" method="get" action="catalog.php">
           <label for="s">Search: </label>
            <input name="s" id="s" type="text"/>
            <input type="submit" value = "GO"/>
           </form>
       </div>
        <!--===============Main-Content================== -->
        <div class="main-content">